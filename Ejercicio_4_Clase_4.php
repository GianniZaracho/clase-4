<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description">
		<title>Mi primer script PHP</title>
    <style type="text/css">

    </style>
	</head>
	<body>
		<?php
			$var = 24.5;
			echo $var;
			echo "<br>";
			echo "Tipo de variable: ";
			echo gettype($var), "\n";
			echo "<br>";
			$var = "HOLA";
			echo "<br>";
			echo $var;
			echo "<br>";
			echo "Tipo de variable: ";
			echo gettype($var), "\n";
			echo "<br>";
			settype($var, 'interger');
			echo "<br>"; 
			echo "var_dump: ";
			var_dump($var);
		?>
	</body>
</html>
